package com.gitlab.jeeto.oboco.data.bookscanner;

public enum BookScannerMode {
	CREATE,
	UPDATE
}

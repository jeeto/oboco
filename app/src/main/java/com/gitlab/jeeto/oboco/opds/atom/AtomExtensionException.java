package com.gitlab.jeeto.oboco.opds.atom;

public class AtomExtensionException extends Exception {

    AtomExtensionException(String message, Throwable cause) {
        super(message, cause);
    }
}

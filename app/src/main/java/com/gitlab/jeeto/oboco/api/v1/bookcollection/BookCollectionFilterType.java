package com.gitlab.jeeto.oboco.api.v1.bookcollection;

public enum BookCollectionFilterType {
	ALL,
	NEW,
	LATEST,
	LATEST_READ,
	TO_READ,
	READ,
	READING,
	UNREAD;
}

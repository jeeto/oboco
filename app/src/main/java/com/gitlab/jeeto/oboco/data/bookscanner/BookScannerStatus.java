package com.gitlab.jeeto.oboco.data.bookscanner;

public enum BookScannerStatus {
	STARTING,
	STARTED,
	STOPPING,
	STOPPED;
}
